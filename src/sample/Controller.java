package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
public class Controller {
    public Label monitor;
    public Button btn0;
    public Button btn1;
    public Button btn2;
    public Button btn3;
    public Button btn4;
    public Button btn5;
    public Button btn6;
    public Button btn7;
    public Button btn8;
    public Button btn9;
    public int a = 0,b = 0;
    public void doClick(ActionEvent actionEvent){
        Button button = (Button) actionEvent.getSource();
        String number = monitor.getText();
        monitor.setText(number.concat(button.getText()));
    }
    public void doCompute(ActionEvent actionEvent){
        Button button = (Button) actionEvent.getSource();
        switch (button.getText()){
            case "+":
                a = Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case "-":
                b = Integer.parseInt(monitor.getText());
                monitor.setText(String.valueOf((a+b)));
                break;
            case "*":
                b = Integer.parseInt(monitor.getText());
                monitor.setText(String.valueOf((a*b)));
                break;
            case "/":
                b = Integer.parseInt(monitor.getText());
                monitor.setText(String.valueOf((a/b)));
                break;
        }
    }
}
